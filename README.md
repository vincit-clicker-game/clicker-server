# Vincit summer job project submission

The demo is deployed to aws. Url: http://vinci-demo.s3.eu-north-1.amazonaws.com/index.html 
The frontend is currently set up as a submodule. The git repo for the project is available at https://gitlab.com/vincit-clicker-game/clicker-front

## General

### Features
* Multiplayer
* Scalable
* Supports custom games
* Truly cross platform (mobile & web, both native)
* Low latency with streams
* Data integrity with mongo replica sets
* Other features as per task description

### Requirements
* Docker
* Docker compose
* flutter beta

### Goals for this project
1. Scalable
2. Use as many hipster technologies as possible
3. Streams are good. Use many streams.

### Technologies
* Docker
* Express
* MongoDB
* Replica set (for mongo)
* Flutter
* JWT
* AWS (ecs/fargate and documentDb)
* Socket.io

### Division of labor
* Front end
    * Me
* Backend
    * Me
* Deployment
    * Me

## Development

### Getting started
1. Clone the repo
2. Run `git submodule init && git submodule update`
3. Go to ./front/vincit_demo and `run flutter build web`
4. Remove .example ending from the .env files. The configuration should work out of the box. However, should you want to change the values feel free. Just make sure both env files are in sync. Note that db passwords are not used in local environment
5. Go to front/vincit_demo/lib/providers/api.dart and change the api url to corresbond to the platform you are running on. The url should start with http://localhost if running in web and http://10.0.2.2 if running in emulator.
6. Start the local stack by running `docker-compose up` in project root

### Known bugs
1. Some flutter for web bugs caused by the immaturity of the platofom

## Misc

### Note
The api for this server is mixed REST and streams. The reason is that my frontend socketJs library has a bug preventing disconnects. Therefore, a lot more functionality is baked into the streams than planned

### ToDo
1. Add the ability for admin to create custom games for the admin (WIP, almost done)
2. Real mobile ui 

### About the scalability
Due to the use of change streams you can in theory deploy an infinite number of instances. This is perfect for, say the use of load balancers. 
In a real world situation one game should never run on differend instances (possible in this setup) due to the lag introduced by mongo change streams. 
However, just for the demo sake it's fine. The score is not send before receiving change stream. This insures that all the players receive it at the same time.

## Why

### Why not lambda?
I had already done some projects with lambda so I wanted to try ecs

### Why express? Shouldn't you never run express in production?
While single threaded, express is quite fast for a server. Also, the framework is really lightweight making it easy to build a simple rest api compared to something heaver like django or even flask.
It's true that the nature of express leads to it crashing compelitely. However, we can use something like pm2 to get rid of that behavior.

### Why streams? Isn't the overhead for http requests almost nonexistent in http/2?
With http polling we will always get some kind of delay and cause unnecessary server load.
Long polling would have been an option, however, streams allowed me to simplify the project architecture.

### Why MongoDb? Why use fake sql when the real one exists? Moreover, documentDb costs some real money in AWS
Mongo works well for quick 'n dirty projects like this one. The ability to change the schema on the fly is a major factor.
Also, change streams are nice (Also in some sql servers ik). Mongo does cost some more than say Aurora Mysql but it's worth the cost (because of the short-term nature of this project).

### Why docker?
You know as well as I do that docker has become the de facto container service / platform.
Dockerised projects are easy to share and run almost everywhere (should you manage to install docker on windows :)
Also, deploying is a breeze as all I need to push to Aws (fargate) is the image and env.

### Why Flutter?
You wanted the front end to be build with a modern technology. I delivered.
Flutter is so new, in fact, that it wasn't even included in your list of modern technologies.
Also, Flutter is amazingly fast to develop as it does JIT in dev and AOT in prod.
It also compiles to native desktop, mobile, web, microcontrollers etc.
Even so, I'm allergic to the idea that there's some kind of a javascript bridge / interpreter in my app even if it doesn't cause any overhead (looking at you, React Native).

### Why JWT?
I don't want to save a token to my db. Also, I like cookies (tokens are saved to local storage tbh).