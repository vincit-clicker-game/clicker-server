import express from "express"
import cors from "cors"
import socketio from 'socket.io'
import http from "http"

import { login, createUser, verifyRequest } from "./auth/auth.js"
import { authenticateStream, handleConnection } from "./socket/socket.js"
import { onGameUpdate } from "./game/gameLogic.js"

import Driver from "./db/driver.js"

const port = process.env.ENVINRONMENT == "dev" ? 5001 : 80
const driver = new Driver()
const app = express()
const server = http.createServer(app)
const io = socketio(server)

//In prod static is served by an s3 instance
if (process.env.ENVINRONMENT === "dev") {
    app.use(express.static('../static'))
}

//We can call driver before init
driver.init().then(async () => {
    let games = await driver.getGame()
    if (games.length === 0) {
        //There should always be an active game
        await driver.createDefaultGame()
        console.log("Created default game")

    }
    const admins = await driver.getUsers({ admin: true })
    if (admins.length === 0) {
        if (!process.env.ADMIN_PASSWORD || !process.env.ADMIN_USERNAME) {
            throw Exception("Admin username and pass must be set!")
        }

        await driver.createUser({
            username: process.env.ADMIN_USERNAME,
            password: process.env.ADMIN_PASSWORD,
            admin: true,
            coins: 0
        })
        console.log("Created default admin account")
    }
    driver.addOnGameChangeListener((game) => {
        onGameUpdate(game, io, driver)
    })
})

//Init streams
io.use(authenticateStream(driver))
    .on('connection', handleConnection(driver, io));

//Middleware
app.use(cors())
app.use(verifyRequest(driver))
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//Test endpoint
app.get("/", (req, res) => {
    res.send("Hello");
})

//Endpoints
app.post("/createUser", async (req, res) => {
    await createUser(req, res, driver)
})

//Returns self
app.post("/user", async (req, res) => {
    if (!req.user) {
        res.status(400)
        res.end()
    } else {
        res.json(req.user)
    }
    //Req.user is attached by auth middleware. See auth.js for more.
})

//Retuns available games
app.post("/games", async (req, res) => {
    if (!req.user) {
        //Must be authenticated
        res.status(400)
        res.end()
        return
    }

    let games = await driver.getGame()
    games = games.map(game => ({ name: game.name, prizes: game.prizes }))
    //Ignore dates for now
    //Todo: implement game start/end
    res.json(games)
})

app.post("/login", async (req, res) => {
    let result;
    try {
        result = await login(req.body, driver)
        if (result['status']) {
            //We have an error here
            res.status(result['status'])
            res.send("Invalid username or password").end()
        } else {
            res.json(result)
        }
    } catch (e) {
        console.log(e)
        res.status(400).end()
    }
})

console.log("Server running at " + port)
server.listen(port);
