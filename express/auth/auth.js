import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"


export const login = async (credentials, driver) => {

    let user = await driver.getUser({
        username: credentials.username
    }).catch((err) => {
        console.log("Error retreiving user for login", err)
        return {
            status: 403
        }
    })

    if (!user) return {
        status: 403
    }

    //Bcrypt doesn't support promises
    const response = await new Promise((resolve, reject) => {
        bcrypt.compare(credentials.password, user.password, function (err, isMatch) {
            if (isMatch) {
                const token = jwt.sign({ username: user.username }, process.env.TOKEN)
                resolve({ userId: user.id, token: token, coins: user.coins })
            }
            else {
                //Don't reject as we still want a response
                resolve({ status: 403 })
            }
        })
    })
    return response
}


export const createUser = async (req, res, driver) => {

    if (req.body.password && req.body.username) {
        try {
            //We don't want anyone to grant themselves admin rights
            delete req.body.admin
            await driver.createUser(req.body)
            res.status(200).end()
        } catch (e) {
            //Thrown if username already exists
            //Bad practice ik but due to the tight schedule skipping username check makes sense
            res.status(400).end()
        }
    } else {
        res.status(400).end()
    }
}

export const verifyUser = (driver, token) => {
    return new Promise((resolve, reject) => {
        try {
            jwt.verify(token, process.env.TOKEN, function (err, payload) {
                if (payload) {
                    if (payload.username === undefined) {
                        reject()
                    }
                    driver.getUser({ username: payload.username }, "username coins game admin").then(
                        (doc) => {
                            resolve(doc)
                        }
                    ).catch((e) => {

                        reject()
                    })
                } else {
                    reject()
                }
            })
        } catch (e) {

            reject()
        }
    })

}

export const verifyRequest = (driver) => (req, res, next) => {

    let token;
    //Auth might not be required
    if (req.headers.authorization) {
        token = req.headers.authorization.split(" ")[1]
    }

    verifyUser(driver, token).then(result => {
        req.user = result;
        next()
    }).catch((_) => {
        next()
    })
}