import { verifyUser } from "../auth/auth.js"
import { onClick, getClicksUntilNextPrize, sendScoreBoard } from "../game/gameLogic.js"

//Socket logic here. For sending scoreboard etc. see gameLogic.js

export const authenticateStream = (driver) => async (socket, next) => {

    console.log("Authenticating user")

    if (socket.handshake.query && socket.handshake.query.token) {
        const result = await verifyUser(driver, socket.handshake.query.token).catch((_) => {
            next(new Error('Authentication error'));
        })

        socket.user = result
        next()

    } else {
        next(new Error('Authentication error'));
    }
}

//Closure due to passing this as a param to socketio
export const handleConnection = (driver, io) => (socket) => {

    socket.emit("tokens", socket.user.coins)

    //Handle socket events
    //Socket here is a single socket

    //Called when stream connection is lost
    socket.on("disconnect", async () => {
        console.log(socket.user.username, "disconnect")
        await driver.unassignGameFromUser({ username: socket.user.username })
    })


    //Called when a user logs out or leaves a game
    socket.on("leaveGame", async () => {
        console.log(socket.user.username, "leaveGame")
        await driver.unassignGameFromUser({ username: socket.user.username })
        await driver.changeGamePlayerCount({ name: socket.game.name }, -1)
        delete socket.game
    })

    //Reset user tokens to 20
    socket.on("resetTokens", () => {
        driver.resetUserTokens({ username: socket.user.username })
        socket.emit("tokens", 20)
    })

    //Called when user joins a  game
    socket.on("joinGame", async (params) => {

        console.log(socket.user.username, "joinGame")

        const game = await driver.getGame({ name: params.game })
        await driver.assignGameToUser({ username: socket.user.username }, game[0].name)
        await driver.changeGamePlayerCount({ name: game[0].name }, 1)
        socket.game = game[0]

        socket.emit("nextPrize:" + socket.game.name, getClicksUntilNextPrize(socket.game))
        socket.emit("tokens", socket.user.coins)

        //Connected-event tells the front that game joining was successful
        socket.emit("connected")
    })

    socket.on("logOut", async () => {

        console.log(socket.user.username, "logOut")
        await driver.unassignGameFromUser({ username: socket.user.username })

        sendScoreBoard(socket.game, io, driver)
        if (socket.game) {
            await driver.changeGamePlayerCount({ name: game[0].name }, -1)
        }

        delete socket.user
        delete socket.game
    })

    //Called when user relogins
    //It's really not changeUser but a login via stream. See README.md for explanation
    socket.on("changeUser", async (params) => {
        console.log("changeUser")
        const token = params.token
        const user = await verifyUser(driver, token).catch((_) => {
            //This is ignored in frontend for now
            socket.emit("error", "Error changing user")
        })
        socket.user = user
    })

    //Small but might click function
    socket.on("click", () => {
        onClick(driver, socket)
    })

}