import mongoose from 'mongoose';
import bcrypt from "bcrypt"
import fs from "fs"
import db from "mongodb"



const MongoClient = db.MongoClient

//Some info:
//The db driver is actually no driver since it doesn't expose any low level functions
//Instead I found it simpler and less error prone to provide high level functions
//This also provides little protection against (no)sql attacts as we always set spesific fields and never modify the whole object
//Graphql would have been nice extra convenience but I didn't see much point implementing it because most of the communication is done via web sockets

//The initialization method for this db is total crap. It actually uses two different mongo libraries, one of which is build on top the other.
//The reason is that aws requires some non-standard initialization not supported by mongoose.
//The whole infrastructure was build on top of mongoose so I had no time to change. 
//Moreover, the con(s) of this implementation are small. Mongoose already relies on mongodb so no extra space is used.
//1. Makes the code harder to understand.

class Driver {

    userModel;
    gameModel;

    init = async () => {

        //Our init is async. Therefore, no constrcutor

        if (process.env.ENVINRONMENT === "dev") {

            try {
                await mongoose.connect(process.env.DB_ADDR, {
                    useNewUrlParser: true,
                    useUnifiedTopology: true,
                    useCreateIndex: true,
                    replicaSet: "rs0"
                });
            } catch (e) {
                console.log(e)
            }

        } else {
            
            //Prod creds are supplied in the url
            //Aws tls cert. Please download from aws website
            const ca = [fs.readFileSync("rds-combined-ca-bundle.pem")];

            //Incredibly stupid implementation
            //Enable change streams
            const conn = await MongoClient.connect(process.env.DB_ADDR, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                sslValidate: true,
                sslCA: ca,
                useNewUrlParser: true,
            });

            const db = conn.db('main');

            await db.executeDbAdminCommand({
                modifyChangeStreams: 1,
                database: "main",
                collection: "",
                enable: true
            });

            await conn.close();

            //Actual connection
            await mongoose.connect(process.env.DB_ADDR, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                sslValidate: true,
                dbName: "main",
                sslCA: ca,
                useNewUrlParser: true,

            });
        }


        console.log("Connected to the db")

        const Schema = mongoose.Schema;

        //Pass is hashed don't worry
        const userSchema = new Schema({
            username: {
                type: String,
                unique: true,
                trim: true,
                maxlength: 15,
                require: true,
                index: true,
            },
            admin: {
                type: Boolean,
                default: false,
            },
            game: String,
            password: String,
            coins: Number,
        });

        //GameSchema contains some fields not used at the moment
        const gameSchema = new Schema({
            starts: Date,
            ends: Date,
            counter: Number,
            playerCount: {
                type: Number,
                default: 0
            },
            url: String,
            creator: String,
            name: {
                type: String,
                unique: true,
                trim: true,
                maxlength: 15,
                require: true,
                index: true,
            },
            prizes: [
                {
                    nth: { type: Number },
                    prize: { type: Number },
                }
            ]
        });

        this.userModel = mongoose.model('user', userSchema);
        this.gameModel = mongoose.model('game', gameSchema);
    }

    createDefaultGame = async () => {
        //Create the default game to the spec
        //Feel free to modify
        const game = new this.gameModel({
            starts: new Date(),
            counter: 0,
            url: "",
            creator: "Admin",
            name: "Default game",
            prizes: [
                {
                    nth: 10,
                    prize: 5,
                },
                {
                    nth: 100,
                    prize: 40,
                },
                {
                    nth: 500,
                    prize: 250,
                },
            ]
        })

        await game.save()
    }

    getGame = async (params) => {
        let result = await this.gameModel.find({ ...params }).exec()
        return result
    }

    getUser = async (params, fields = "") => {
        const result = await this.userModel.findOne({ ...params }, fields).exec()
        return result
    }

    resetCounter = async (params) => {
        const game = await this.gameModel.findOne({ ...params }).exec()
        game.counter = 0
        await game.save()
    }

    getUsers = async (params, fields = "") => {
        const result = await this.userModel.find({ ...params }, fields).exec()
        return result
    }

    assignGameToUser = async (params, game) => {
        //Game is a string
        //Params are for finding the user
        const user = await this.userModel.findOne({ ...params }).exec()
        user.game = game
        await user.save()
    }

    unassignGameFromUser = async (params) => {
        const user = await this.userModel.findOne({ ...params }).exec()
        user.game = null
        await user.save()
    }

    changeGamePlayerCount = async (params, amount) => {
        const game = await this.gameModel.findOne({ ...params }).exec()
        game.playerCount += amount
        await game.save()
    }

    addCoinsToUser = async (userParams, coins) => {
        const user = await this.userModel.findOne({ ...userParams }).exec()
        user.coins += coins
        await user.save()
    }

    decreaseCoinsAndIncreaseCounter = async (userParams, gameParams) => {
        const user = await this.userModel.findOne({ ...userParams }).exec()
        user.coins--
        const game = await this.gameModel.findOne({ ...gameParams }).exec()
        game.counter++
        await Promise.all([
            game.save(),
            user.save()
        ])
    }

    resetUserTokens = async (userParams) => {
        const user = await this.userModel.findOne({ ...userParams }).exec()
        //20 is a hard coded magic number ik
        user.coins = 20;
        await user.save()
    }

    addOnGameChangeListener = (onChange) => {
        //Using mongo change streams here
        //Replica set must be in use for this function to function.
        //Should work out of the box with default docker config
        this.gameModel.watch().
            on('change', async data => {
                console.log("Received change stream")
                if (data["operationType"] === "update") {
                    const game = await this.getGame({ _id: data.documentKey["_id"] })
                    onChange(game[0])
                }
            });
    }

    createUser = async (params) => {
        //See? I promised the password is hashed
        //Todo: Increase rounds in year 2050 to keep up with increasingly powerfull processors
        const hashed = await bcrypt.hash(params.password, 10)
        const user = new this.userModel({
            username: params.username,
            coins: 20,
            password: hashed,
            admin: params.admin ? params.admin : false
        })

        await user.save()
    }
}
export default Driver