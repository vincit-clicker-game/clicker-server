//Most of the game locig lies here
//Some bits and pieces are also in socket.jsc (the file, not the library)

export const onClick = async (driver, socket) => {
    //socket.user might be out-of-date
    const user = await driver.getUser({ username: socket.user.username })
    let game = await driver.getGame({ name: socket.game.name })
    game = game[0]

    if (user.coins > 0) {
        await driver.decreaseCoinsAndIncreaseCounter({ username: user.username }, { name: game.name })
    } else {
        throw Exception("Not enough coins")
    }

    game.counter++

    const prize = getPrize(game)

    //Free up some (very little) space and avoid big numbers
    if (canResetCounter(game)) {
        driver.resetCounter({ name: game.name })
    }

    if (prize > 0) {
        await driver.addCoinsToUser({ username: user.username }, prize)
    }

    socket.emit("tokens", user.coins - 1 + prize)
    socket.emit("clickResponse", prize)

}

//Test if every prize = 0 mod counter
const canResetCounter = game => game.prizes.every(prize => game.counter % prize.nth === 0)

const getPrize = (game) => game.prizes.reduce((maxPrize, currPrize) => {
    if (game.counter % currPrize.nth === 0 && maxPrize < currPrize.prize) {
        return currPrize.prize
    }
    return maxPrize
}, 0)

//Socket can be either one socket or the io object 
export const sendScoreBoard = async (game, socket, driver) => {

    if (!game) return

    let name = game.name;

    driver.getUsers({ game: name }, "username coins").then(users => {
        console.log("Scoreboard", users)
        //Send scoreboard only to specified game
        socket.emit("users:" + game.name, users)
    })
}

//Calculate clicks to prize of every prize and return the smallest one
export const getClicksUntilNextPrize = (game) => game.prizes.reduce((minClicks, currPrize) => {
    const until = currPrize.nth - game.counter % currPrize.nth
    if (until !== 0 && (until < minClicks || minClicks === 0)) return until
    return minClicks
}, 0)

//Called every time someone clicks the button or a player joins a game. The event cames from mongo change stream
export const onGameUpdate = (game, io, driver) => {
    sendScoreBoard(game, io, driver)
    io.emit("nextPrize:" + game.name, getClicksUntilNextPrize(game))
}