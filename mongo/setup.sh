if [ -f /replicated.txt ]; then
  echo ""
else
  sleep 5
  mongo db:27017 init.js
  touch /replicated.txt
fi